{ stdenv, pkgs }:

# You can see the plugin that you want with this command : nix-env -f '<nixpkgs>' -qaP -A eclipses.plugins --description
# You can also check the type of eclipse that you want with this command : nix-env -f '<nixpkgs>' -qaP -A eclipses --description

stdenv.mkDerivation {
    name = "AOO";
    shellHook = ''
      echo -e "\e[32m=============================================\e[0m"
      echo -e "\e[32m=== Welcome to AOO environment ===\e[0m"
      echo -e "\e[32m=============================================\e[0m"
    '';
    buildInputs = [
        pkgs.eclipses.eclipse-java 
    ];
}